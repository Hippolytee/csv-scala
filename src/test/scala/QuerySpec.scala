import org.scalatest._

import parse._
import query.Query

class QuerySpec() extends FlatSpec {

  val countries = new Parser[Country]("csv/countries.csv", {l => Country(l)}).lines
  val runways = new Parser[Runway]("csv/runways.csv", {l => Runway(l)}).lines
  val airports = new Parser[Airport]("csv/airports.csv", {l => Airport(l)}).lines

  val query = new Query(runways, airports, countries)

  "Query.get_country" should "find the correct country with code input" in {
    val result = query.get_country("CY").getOrElse(null)
    assert(result != null)
    assert(result.name == "Cyprus")
  }

  it should "find the correct country with name input" in {
    val result : Country = query.get_country("Cyprus").getOrElse(null)
    assert(result != null)
    assert(result.code == "CY")
  }

  it should "find the correct country with partial name input" in {
    val result : Country = query.get_country("Cypr").getOrElse(null)
    assert(result != null)
    assert(result.code == "CY")
  }

  it should "return None if it doesn't know the country" in {
    val result = query.get_country("???")
    assert(result == None)
  }

  "Query.get_airports" should "return all airports of a given country" in {
    val country = countries.find(e => e.code == "CY").getOrElse(null)
    val result = query.get_airports(country)
    assert(result.length == 2)
  }

  "Query.get_runways" should "return all runways for an airport" in {
    val airport = airports.find(e => e.ident == "LCEN").getOrElse(null)
    val result = query.get_runways(airport)
    assert(result.length == 1)
  }
   

}
