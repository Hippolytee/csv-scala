import org.scalatest._

import parse._
import report.Report

class ReportSpec() extends FlatSpec {

  val countries = new Parser[Country]("csv/countries.csv", {l => Country(l)}).lines
  val runways = new Parser[Runway]("csv/runways.csv", {l => Runway(l)}).lines
  val airports = new Parser[Airport]("csv/airports.csv", {l => Airport(l)}).lines

  val report = new Report(runways, airports, countries)

  def is_sorted(l : List[Int]) : Boolean = l match {
    case (Nil) => true
    case (_::Nil) => true
    case (a::b::t) if a <= b => is_sorted(b :: t)
    case _ => false
  }

  "Report.get_airports" should "sort all countries by number of airports (ascending order)" in {
    val result = report.get_airports()
    assert(is_sorted(result.map(x => x._2)))
  }

  "Report.get_country" should "return a country's name given it's code" in {
    val result = report.get_country("CY")
    assert(result == "Cyprus (CY)")
  }

  it should "return 'Unknown country code: $code' when the code is unknown" in {
    val result = report.get_country("CT")
    assert(result == s"Unknown country code: CT")
  }

  "Report.get_surfaces" should "return a map of surfaces by countries" in {
    val result = report.get_surfaces()
    assert(result.length == 4)
    assert(result.map(x => x._2)
                 .filter(x => x.contains("PIZZA") ||
                              x.contains("WATERMELON") ||
                              x.contains("SAND"))
                 .length == 3)
  }

  "Report.get_latitudes" should "sort all latitudes by commonness (descending order)" in {
    val result = report.get_latitudes()
    assert(is_sorted(result.map(x => x._2).reverse))
  }
}
