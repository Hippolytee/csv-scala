import org.scalatest._

import parse._

class ParseSpec() extends FlatSpec {

  "Parse" should "return correct List[Country]" in {
    val countries = new Parser[Country]("csv/countries.csv", {l => Country(l)}).lines
    assert(countries.length == 4)

    val country_codes = countries.map(x => x.code)
    assert(country_codes.contains("CY"))
    assert(country_codes.contains("IT"))
    assert(country_codes.contains("UZ"))
    assert(country_codes.contains("PY"))

    val country_names= countries.map(x => x.name)
    assert(country_names.contains("Cyprus"))
    assert(country_names.contains("Italy"))
    assert(country_names.contains("Uzbekistan"))
    assert(country_names.contains("Paraguay"))
  }

  it should "return correct List[Runway]" in {
    val runways = new Parser[Runway]("csv/runways.csv", {l => Runway(l)}).lines
    assert(runways.length == 5)

    val airport_idents = runways.map(x => x.airport_ident)
    assert(airport_idents.contains("CY-0001"))
    assert(airport_idents.contains("LCEN"))
    assert(airport_idents.contains("UA66"))
    assert(airport_idents.contains("LIRF"))
    assert(airport_idents.contains("PY-0006"))

    val surfaces = runways.map(x => x.surface)
    assert(surfaces.contains("GRASS"))
    assert(surfaces.contains("SAND"))
    assert(surfaces.contains("PIZZA"))
    assert(surfaces.contains("WATERMELON"))
    assert(surfaces.contains("PAVEMENT"))

    val latitudes = runways.map(x => x.le_ident)
    assert(latitudes.contains("H1"))
    assert(latitudes.contains("C4"))
    assert(latitudes.contains("VERY"))
    assert(latitudes.contains("EL"))
  }

  it should "return correct List[Airport]" in {
    val airports = new Parser[Airport]("csv/airports.csv", {l => Airport(l)}).lines
    assert(airports.length == 5)

    val airport_idents = airports.map(x => x.ident)
    assert(airport_idents.contains("CY-0001"))
    assert(airport_idents.contains("LCEN"))
    assert(airport_idents.contains("UA66"))
    assert(airport_idents.contains("LIRF"))
    assert(airport_idents.contains("PY-0006"))
  }
}
