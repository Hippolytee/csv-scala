package query


import parse.Airport
import parse.Country
import parse.Runway

case class Query(runways : List[Runway], airports : List[Airport], countries : List[Country]) {
  def print_possibilities(l: List[Country]): Unit = l match{
       case Nil =>
       case h::tail => { print_possibilities(tail)
                         println("  - " + h.name)
                       }
  }


  def print_runways(airport : Airport, airport_runways : List[Runway]) : Unit = {
       println("  - " + airport.name)
       println("    - Runways : ")
       airport_runways.map(runway => println("      - " + runway.surface))
  }

  def get_runways(airport : Airport) =
    runways.filter(runway => runway.airport_ident.equalsIgnoreCase(airport.ident))

  def get_airports(country : Country)  =
    airports.filter(airport => airport.iso_country.equalsIgnoreCase(country.code))

  def get_country(str : String) : Option[Country] =
    countries.find(e => str.equalsIgnoreCase(e.code)) match{ 
    case None => countries.filter(_.name.toLowerCase().startsWith(str.toLowerCase())) match {
                 case Nil => { println("\nUnknown country\n")
                               None
                             }
    case h::Nil => Some(h) 
    case h::tail => { println("\nDid you mean one of these?\n")
                      print_possibilities(h::tail)
                      println("\n")
                      None
                    }
    }
    case Some(c) => Some(c)
}

  def query(): Unit = {
    println("Enter a country name or code : ")
    val input = readLine("> ")
    val country = get_country(input)
    country match {
      case None => query()
      case Some(c) => {
        println("Airports : ")
        get_airports(c).map(e => print_runways(e, get_runways(e)))
      }
    }
  }
}
