package report

import parse.Parser
import parse.Airport
import parse.Country
import parse.Runway

case class Report(runways : List[Runway], airports : List[Airport], countries : List[Country]) {

  def get_country(code : String) : String =
    countries.find(country => country.code == code) match {
      case Some(country) => country.name + s" ($code)"
      case _ => s"Unknown country code: $code"
    }

  def get_airports() : List[(String, Int)] =
    airports.groupBy(_.iso_country)
            .toList
            .map(x => (x._1, x._2.length))
            .sortBy(_._2)

  def print_airports(airportsByCountry : List[(String, Int)]) : Unit = {
    println("Countries with most airports:")
    val length = airportsByCountry.length
    airportsByCountry.slice(length - 11, length - 1)
                     .reverse
                     .map(x => println(get_country(x._1) + ": " + x._2))

    println("\nCountries with least airports:")
    airportsByCountry.slice(0, 10)
                     .map(x => println(get_country(x._1) + ": " + x._2))
  }

  def get_surfaces() : List[(String, List[String])] = {
    val runways_by_airport = runways.groupBy(_.airport_ident)

    airports.groupBy(_.iso_country)
            .toList
            .map(x => (x._1, x._2.filter(e => runways_by_airport.contains(e.ident))
                                 .flatMap(e => runways_by_airport(e.ident))
                                 .map(r => r.surface)
                                 .distinct))
  }

  def print_surfaces(runways : List[(String, List[String])]) : Unit = {
    println("Runways by country:")
    runways.map(x => get_country(x._1)
                     + " ==> "
                     + x._2.foldLeft("")((acc, e) => acc + e + ", "))
           .map(x => println(x.replaceAll(", $", "")))
  }

  def get_latitudes() : List[(String, Int)] = {
    runways.groupBy(_.le_ident)
           .toList
           .map(x => (x._1, x._2.length))
           .sortBy(_._2)
           .reverse
           .slice(0, 10)
  }

  def print_latitudes(latitudes : List[(String, Int)]) : Unit = {
    println("Top 10 most common latitudes:")
    latitudes.map(x => println(x._1 + ": " + x._2))
  }

  def report() = {
    print_airports(get_airports())
    println()
    print_surfaces(get_surfaces())
    println()
    print_latitudes(get_latitudes())
  }
}
