package parse

import scala.io.Source


case class Country(l : List[String]) {
  val id = l(0)
  val code = l(1)
  val name = l(2)
  val continent = l(3)
  val wikipedia_link = l(4)
  val keywords = l(5)
}

case class Airport(l : List[String]) {
  val id = l(0)
  val ident = l(1)
  val airport_type = l(2)
  val name = l(3)
  val latitude_deg = l(4)
  val longitude_deg = l(5)
  val elevation_ft = l(6)
  val continent = l(7)
  val iso_country = l(8)
  val iso_region = l(9)
  val municipality = l(10)
  val scheduled_service = l(11)
  val gps_code = l(12)
  val iata_code = l(13)
  val local_code = l(14)
  val home_link = l(15)
  val wikipedia_link = l(16)
  val keywords = l(17)
}

case class Runway(l : List[String]) {
  val id = l(0)
  val airport_ref = l(1)
  val airport_ident = l(2)
  val length_ft = l(3)
  val width_ft = l(4)
  val surface = l(5)
  val lighted = l(6)
  val closed = l(7)
  val le_ident = l(8)
  val le_latitude_deg = l(9)
  val le_longitude_deg = l(10)
  val le_elevation_ft = l(11)
  val le_heading_degT = l(12)
  val le_displaced_threshold_ft = l(13)
  val he_ident = l(14)
  val he_latitude_deg = l(15)
  val he_longitude_def = l(16)
  val he_elevation_ft = l(17)
  val he_heading_degT = l(18)
  val he_displaced_threshold_ft = l(19)
}

class Parser[A](file : String, fac: List[String] => A) {
  val lines = createObjects(Source.fromResource(file)
                                  .getLines
                                  .toList
                                  .drop(1)
                                  .map(_.split(",", -1)
                                        .toList
                                        .map(e => e.replaceAll("^\"|\"$", ""))))

  def createObjects(l : List[List[String]]) : List[A] =
    l.foldLeft(List[A]())((acc, e) => fac(e) :: acc)
}




