import parse.Parser
import parse.Airport
import parse.Country
import parse.Runway
import report.Report
import query.Query

object Main extends App {
  val runways = new Parser[Runway]("csv/runways.csv", {l => Runway(l)}).lines
  val countries = new Parser[Country]("csv/countries.csv", {l => Country(l)}).lines
  val airports = new Parser[Airport]("csv/airports.csv", {l => Airport(l)}).lines

  val report = new Report(runways, airports, countries)
  val query = new Query(runways, airports, countries)

  def run() : Unit = {
    println("\nPlease type the name of the action you want to execute:")
    println("  - query")
    println("  - report")
    println("  - exit\n")

    val choice = readLine("> ")
    println()
    choice match {
      case "query" => { query.query(); run() }
      case "report" => { report.report(); run() }
      case "exit" => println("Program exited successfuly.")
      case _ => { println(s"Unknown action '$choice'\n"); run() }
    }
  }

  run()
}
